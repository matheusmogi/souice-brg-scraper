﻿using System.Linq;
using Moq;
using NUnit.Framework;
using Scraper.Handlers;
using Scraper.Pdf;

namespace Scraper.Tests
{
    [TestFixture]
    public class WhenAPdfIsExtracted
    {
        [Test]
        public void ShouldHandlerFiscalProducts()
        {
            var pdfService = new Mock<IPdfService>();
            pdfService.Setup(x => x.GetText(It.IsAny<byte[]>()))
                .Returns(PdfTextSamples.FiscalTextPdfSample);
            var handler = new FiscalHandler(pdfService.Object);

           var products= handler.Convert(It.IsAny<byte[]>());
           Assert.That(products.Count(), Is.EqualTo(20));
           Assert.That(products[0].Code, Is.EqualTo("003026"));
           Assert.That(products[0].Name, Is.EqualTo("CONE BRIGADEIRO MAX C/08 UN. GN\r"));
           Assert.That(products[0].Tax, Is.EqualTo(4.81m));
           Assert.That(products[0].Total, Is.EqualTo(38.16m));
           Assert.That(products[0].Count, Is.EqualTo(3m));
           Assert.That(products[0].Pack, Is.EqualTo(8));
        }
        
        [Test]
        public void ShouldHandlerGerencialProducts()
        {
            var pdfService = new Mock<IPdfService>();
            pdfService.Setup(x => x.GetText(It.IsAny<byte[]>()))
                .Returns(PdfTextSamples.GerencialTextPdfSample);
            var handler = new GerencialHandler(pdfService.Object);

            var products= handler.Convert(It.IsAny<byte[]>());
            Assert.That(products.Count(), Is.EqualTo(22));
            Assert.That(products[0].Code, Is.EqualTo("006803"));
            Assert.That(products[0].Tax, Is.EqualTo(0));
            Assert.That(products[0].Total, Is.EqualTo(37.80m));
            Assert.That(products[0].Count, Is.EqualTo(1m));
            Assert.That(products[0].Pack, Is.EqualTo(30));
        }

        
    }
}