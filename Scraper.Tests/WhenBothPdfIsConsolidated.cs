﻿using Moq;
using NUnit.Framework;
using Scraper.Handlers;
using Scraper.Pdf;

namespace Scraper.Tests
{
    public class WhenBothPdfIsConsolidated
    {
        [Test]
        public void ShouldReturnCsvWithProducts()
        {
            var pdfService = new PdfService();

            var consolidateService =
                new ConsolidateService(new FiscalHandler(pdfService), new GerencialHandler(pdfService));

            var fiscal = System.IO.File.ReadAllBytes(@"D:\Projetos\BRGScraping\Scraper\Pdf\Samples\FISCAL.pdf");
            var gerencial = System.IO.File.ReadAllBytes(@"D:\Projetos\BRGScraping\Scraper\Pdf\Samples\GERENCIAL.pdf");

            var response = consolidateService.Generate(fiscal, gerencial);
            Assert.That(response.Length, Is.GreaterThan(0));
        }
    }
}