﻿using System;
using NUnit.Framework;
using Scraper.Pdf;

namespace Scraper.Tests
{
    [TestFixture]
    public class WhenAPdfIsProvided
    {
        [TestCase(@"D:\Projetos\BRGScraping\Scraper\Pdf\Samples\FISCAL.pdf")]
        [TestCase(@"D:\Projetos\BRGScraping\Scraper\Pdf\Samples\GERENCIAL.pdf")]
        public void ShouldReturnHtmlAsString(string path)
        {
            var bytes = System.IO.File.ReadAllBytes(path);
            var pdfHandler = new PdfService();
            
            var result = pdfHandler.GetText(bytes);
            
            Assert.That(result.Length, Is.GreaterThan(0));
        }
    }
}