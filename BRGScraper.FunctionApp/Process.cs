﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Scraper;

namespace BRGScraper.FunctionApp
{
    public class Process
    {
        private readonly IConsolidateService consolidateService;
        private string storageConnectionString;

        public Process(IConsolidateService consolidateService, IConfiguration configuration)
        {
            this.consolidateService = consolidateService;
            storageConnectionString = configuration["storageAccount"];
        }

        [FunctionName("Process")]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]
            HttpRequestMessage req,
            ILogger log)
        {
            var multipartMemoryStreamProvider = new MultipartMemoryStreamProvider();
            await req.Content.ReadAsMultipartAsync(multipartMemoryStreamProvider);
            var fiscal =
                multipartMemoryStreamProvider.Contents.FirstOrDefault(x =>
                    x.Headers.ContentDisposition.Name.ToLower().Contains("fiscal"));
            var gerencial =
                multipartMemoryStreamProvider.Contents.FirstOrDefault(x =>
                    x.Headers.ContentDisposition.Name.ToLower().Contains("gerencial"));

            if (fiscal == null || gerencial == null) return new BadRequestObjectResult("Envie o arquivo FISCAL e GERENCIAL.");

            var csv = consolidateService.Generate(await fiscal.ReadAsByteArrayAsync(),
                await gerencial.ReadAsByteArrayAsync());

            var fileName = await SaveToStorageAccount(csv);

            return new FileContentResult(System.Text.Encoding.Default.GetBytes(csv),
                "application/octet-stream") {
                FileDownloadName = $"{fileName}.csv"
            };
        }

        private async Task<string> SaveToStorageAccount(string csv)
        {
            var fileName = Guid.NewGuid().ToString("d");
            return fileName;
            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            var client = storageAccount.CreateCloudBlobClient();
            var container = client.GetContainerReference("converted");
            await container.CreateIfNotExistsAsync();
            var blob = container.GetBlockBlobReference($"{fileName}.csv");
            await using (var x = await blob.OpenWriteAsync())
            {
                await x.WriteAsync(System.Text.Encoding.Default.GetBytes(csv));
            }

            return fileName;
        }
    }
}