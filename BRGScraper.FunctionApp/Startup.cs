﻿using BRGScraper.FunctionApp;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Scraper;
using Scraper.Handlers;
using Scraper.Pdf;

[assembly: FunctionsStartup(typeof(Startup))]
namespace BRGScraper.FunctionApp
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {            
            builder.Services.AddSingleton<IConsolidateService, ConsolidateService>();
            builder.Services.AddSingleton<IFiscalHandler, FiscalHandler>();
            builder.Services.AddSingleton<IGerencialHandler, GerencialHandler>();
            builder.Services.AddSingleton<IPdfService, PdfService>();
        }
    }
}