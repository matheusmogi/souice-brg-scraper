﻿namespace Scraper.Model
{
    public class Product
    {
        public Product(string code, string name, decimal count, decimal tax, decimal total, int pack)
        {
            Code = code;
            Name = name;
            Count = count;
            Tax = tax;
            Total = total;
            Pack = pack;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public decimal Count { get; private set; }
        public decimal Tax { get; private set; }
        public decimal Total { get; private set; }
        public int Pack { get; private set; }
    }
}