﻿namespace Scraper.Pdf
{
    public class PdfService : IPdfService
    {
        public string GetText(byte[] pdf)
        {
            var pdfContent = "";
            var pdfReader = new iTextSharp.text.pdf.PdfReader(pdf);
            for (var i = 1; i <= pdfReader.NumberOfPages; i++)
            {
                var its = new iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy();
                pdfContent += iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(pdfReader, i, its);
            }

            return pdfContent;
        }
    }

    public interface IPdfService
    {
        string GetText(byte[] pdf);
    }
}