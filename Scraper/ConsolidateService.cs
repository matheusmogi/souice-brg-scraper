﻿using System;
using System.Collections.Generic;
using System.Linq;
using iTextSharp.text;
using Scraper.Handlers;
using Scraper.Pdf;

namespace Scraper
{
    public interface IConsolidateService
    {
        string Generate(byte[] pdfFiscal, byte[] pdfGerencial);
    }

    public class ConsolidateService : IConsolidateService
    {
        private readonly IFiscalHandler fiscalHandler;
        private readonly IGerencialHandler gerencialHandler;

        public ConsolidateService(IFiscalHandler fiscalHandler, IGerencialHandler gerencialHandler)
        {
            this.fiscalHandler = fiscalHandler;
            this.gerencialHandler = gerencialHandler;
        }

        public string Generate(byte[] pdfFiscal, byte[] pdfGerencial)
        {
            var finalProducts = new List<FinalProduct>();
            var fiscalProducts = fiscalHandler.Convert(pdfFiscal);
            var gerencialProducts = gerencialHandler.Convert(pdfGerencial);

            foreach (var gerencialProduct in gerencialProducts)
            {
                var fiscalProduct = fiscalProducts.FirstOrDefault(x => x.Code == gerencialProduct.Code);
                finalProducts.Add(new FinalProduct
                {
                    Code = gerencialProduct.Code,
                    Count = gerencialProduct.Pack * (fiscalProduct == null
                                ? gerencialProduct.Count
                                : gerencialProduct.Count + fiscalProduct.Count),
                    Total = fiscalProduct == null
                        ? gerencialProduct.Total
                        : gerencialProduct.Total + fiscalProduct.Total + fiscalProduct.Tax,
                    ProductName = fiscalProduct?.Name ?? gerencialProduct.Name
                });
            }

            return $"cod;nome;total;qtd;custo unit.\n{string.Join("\n", finalProducts.Select(x => x.ToString()))}";
        }
    }

    public class FinalProduct
    {
        public string Code { get; set; }
        public string ProductName { get; set; }
        public decimal Total { get; set; }
        public decimal Count { get; set; }
        private decimal UnitPrice => Total / Count;

        public override string ToString()
        {
            return $"{Code};{ProductName.Replace(",", ".")};{Total};{Count};{UnitPrice}";
        }
    }
}