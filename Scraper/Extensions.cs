﻿namespace Scraper
{
    public static class Extensions
    {
        public static string ComaToPoint(this string value)
        {
            return value.Replace(",", ".");
        }
    }
}