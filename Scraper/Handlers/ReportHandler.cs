﻿using Scraper.Model;
using Scraper.Pdf;

namespace Scraper.Handlers
{
    public abstract class ReportHandler
    {
        protected string PdfContent;
        public abstract Product[] Convert(byte[] pdf);
    }
}