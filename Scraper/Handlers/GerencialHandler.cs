﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Scraper.Model;
using Scraper.Pdf;

namespace Scraper.Handlers
{
    public class GerencialHandler : ReportHandler, IGerencialHandler
    {
        private readonly IPdfService pdfService;
        private List<Product> products;

        public GerencialHandler(IPdfService pdfService)
        {
            this.pdfService = pdfService;
            products = new List<Product>();
        }

        public override Product[] Convert(byte[] pdf)
        {
            PdfContent = pdfService.GetText(pdf);
            var lines = PdfContent.Split('\n');
            foreach (var line in lines)
            {
                if (!Regex.IsMatch(line, "^00\\d{4}")) continue;
                var code = Regex.Match(line, "^00\\d{4}").Value;
                var count = Regex.Matches(line, "(\\d{1,2} R[$])").First().Value
                    .Replace("R", " ")
                    .Substring(0, 3).Trim();
                var pack = Regex.IsMatch(line, " 4X")
                    ? "4"
                    : Regex.Match(line, "\\d{2,3} UN|\\d{2,3} SOU").Value.Substring(0, 2);
                var values = Regex.Matches(line, "(R[$]\\d{1,4},\\d{2})");

                products.Add(
                    new Product(code,
                        line.Substring(19).Replace(",","."),
                        decimal.Parse(count),
                        0,
                        decimal.Parse(values.Last().Value.Replace("R$", string.Empty)),
                        int.Parse(pack)));
            }

            return products.ToArray();
        }
    }

    public interface IGerencialHandler
    {
        Product[] Convert(byte[] pdf);
    }
}