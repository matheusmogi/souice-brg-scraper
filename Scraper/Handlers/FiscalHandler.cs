﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Scraper.Model;
using Scraper.Pdf;

namespace Scraper.Handlers
{
    public class FiscalHandler : ReportHandler, IFiscalHandler
    {
        private readonly IPdfService pdfService;
        private readonly List<Product> products;

        public FiscalHandler(IPdfService pdfService)
        {
            this.pdfService = pdfService;
            products = new List<Product>();
        }

        private string[] especiais = {"PALETA MORANGO C/LEITE ", "CREME PREP.DE ACAI", "PALETA DE IOGURTE C/FRUTAS","PICOLE EXTR MORANGO COM IOGURTE ","SORVETE 400ML NAPOLITANO ESPECIAL"};
        public override Product[] Convert(byte[] pdf)
        {
            PdfContent = pdfService.GetText(pdf);
            var lines = PdfContent.Split('\n');
            for (var i = 0; i < lines.Length; i++)
            {
                if (!Regex.IsMatch(lines[i], "^00\\d{4}")) continue;
                var code = Regex.Match(lines[i], "^00\\d{4}").Value;
                var count = Regex.Match(lines[i + 1], "^(CX|FD) \\d,\\d{2}").Value.Substring(2);
                var productName = Regex.IsMatch(lines[i - 1], "SORVETE|CONE|PICOLE|TRUFADO|CREME|PALETA")
                    ? lines[i - 1]
                    : lines[i - 2];
                string pack;
                if (especiais.Any(x=>productName.Contains(x)))
                {
                    pack = Regex.Match(lines[i-1], "\\d{2,3} UN|\\d{2,3} SOU").Value.Substring(0, 2);
                    
                }
                else
                {
                    var value = Regex.Match(productName, "MANGA C/ \\d{2,3}").Value;
                    pack = Regex.IsMatch(productName, " 4X| MOUSSE DE")
                        ? "4"
                        : Regex.IsMatch(productName, "MANGA C/ \\d{2,3}") ?
                            value.Substring(value.Length-3, 3).Trim()
                        : Regex.Match(productName, "\\d{2,3} UN|\\d{2,3} SOU").Value.Substring(0, 2);
                }

                products.Add(
                    new Product(code,
                        productName,
                        decimal.Parse(count),
                        decimal.Parse(lines[i + 4]),
                        decimal.Parse(lines[i + 2]),
                        int.Parse(pack)));
            }

            return products.ToArray();
        }
    }

    public interface IFiscalHandler
    {
        Product[] Convert(byte[] pdf);
    }
}